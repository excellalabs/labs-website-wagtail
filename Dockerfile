FROM python:3.6.8-alpine3.9

RUN apk add --no-cache bash postgresql postgresql-dev postgresql-client gcc python3-dev musl-dev libffi-dev git jpeg-dev zlib-dev alpine-sdk xmlsec

ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt /code/requirements.txt
RUN pip install -r /code/requirements.txt
RUN pip install gunicorn

COPY . /code/
WORKDIR /code/

EXPOSE 8000

ADD ./docker-entrypoint.sh /
RUN chmod 755 /docker-entrypoint.sh
CMD ["/docker-entrypoint.sh"]
