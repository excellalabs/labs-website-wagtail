from django.shortcuts import render
from django import views
from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic import TemplateView
from django.http import Http404

from wagtail.contrib.modeladmin.views import EditView
from wagtail.users.models import UserProfile

from . import models
from django.contrib.auth.models import User

class PeoplePageEditView(EditView):

    def check_action_permitted(self, user):
        return user.has_perm('people.can_edit_' + self.get_instance().user.username)

class PeopleDetailView(TemplateView):
    template_name = "people_profile.html"

    def dispatch(self, *args, **kwargs):
        if next((
                    True for profile in models.PeoplePagesSettings.for_site(self.request.site).live_profiles
                    if profile.value.slug == self.kwargs['slug']
                ), False
            ):
            return super().dispatch(*args, **kwargs)    
        raise Http404

    def get_context_data(self, **kwargs):
        contributor = next(
            profile.value for profile in models.PeoplePagesSettings.for_site(self.request.site).live_profiles
            if profile.value.slug == self.kwargs['slug']
        )

        context = super().get_context_data(**kwargs)
        context['profile'] = models.PeoplePageModel.objects.get(user=contributor)
        context['contributions'] = contributor.contributions()
        return context

class PeopleListView(TemplateView):
    template_name = "people_list.html"

    def get_context_data(self, **kwargs):

        live_profiles = models.PeoplePagesSettings.for_site(self.request.site).live_profiles
        people = []

        for block in live_profiles:
            contributor = block.value
            people_page_model = models.PeoplePageModel.objects.get(user=contributor)
            person = {
                'people_page_model': people_page_model,
                'contributor': contributor,
                'url': self.get_url(contributor)
            }

            if hasattr(contributor, 'wagtail_userprofile') and hasattr(contributor.wagtail_userprofile, 'avatar'):
                person.update({'avatar_url': contributor.wagtail_userprofile.avatar.url})

            people.append(person)

        context = super().get_context_data(**kwargs)
        context['profiles'] = people
        return context

    def get_url(self, user):
        return '/people/' + user.slug + '/'