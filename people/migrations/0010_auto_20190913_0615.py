# Generated by Django 2.1.4 on 2019-09-13 10:15

from django.db import migrations, models
import wagtail.core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('people', '0009_auto_20190809_1145'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='peoplepagessettings',
            name='copy',
        ),
        migrations.AddField(
            model_name='peoplepagessettings',
            name='description',
            field=wagtail.core.fields.RichTextField(blank=True, help_text='Description/caption to display at top of list page', null=True),
        ),
        migrations.AddField(
            model_name='peoplepagessettings',
            name='title',
            field=models.CharField(blank=True, max_length=500),
        ),
    ]
