from django.apps import apps
from django.db import models
from django.conf import settings
from django.contrib.auth.models import User, Permission
from django.shortcuts import render
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.utils.text import slugify
from operator import attrgetter

from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel, InlinePanel
from wagtail.core import blocks
from wagtail.core.fields import RichTextField, StreamField
from wagtail.core.models import Page
from wagtail.core import hooks
from wagtail.admin.menu import MenuItem
from wagtail.contrib.settings.models import BaseSetting, register_setting
from wagtail.contrib.modeladmin.options import (
    ModelAdmin, modeladmin_register)
from wagtail.contrib.settings.registry import SettingMenuItem
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.snippets.models import register_snippet
from wagtail.snippets.edit_handlers import SnippetChooserPanel
from wagtailmodelchooser import register_model_chooser
from wagtailmodelchooser.edit_handlers import ModelChooserPanel
from wagtailmodelchooser.blocks import ModelChooserBlock

from home.models import BasePageWithHero
from modelcluster.models import ClusterableModel
from modelcluster.fields import ParentalKey

from .views import PeoplePageEditView

@register_model_chooser
class Contributor(User):
    """A proxy model for User so we can add the register_model_chooser decorator"""

    @classmethod
    def find_contributed_models(cls):
        """
        Find all models with the 'first_published_at' and ('authors' or 'presenters') fields.
        These are the models a contributor can author or present.
        """
        people_fields = ['authors', 'presenters',]
        model_set = set()
        for app_name in apps.app_configs.keys():
            for model in apps.all_models[app_name].values():
                if hasattr(model, 'first_published_at'):
                    for field in people_fields:
                        if hasattr(model, field):
                            model_set.add(model)
        cls.authoring_models = list(model_set)
    
    class Meta:
        proxy = True
    
    def __str__(self):
        # The ``str()`` of your model will be used in the chooser
        return "{} {}".format(self.first_name, self.last_name)
    
    @property
    def slug(self):
        return '{}'.format(slugify(self.email.split('@')[0], allow_unicode=True))

    def contributions(self):
        """
        Return a list of published objects where self is an author or presenter.
        List sorted by first_published_at date
        """
        contributions_set = set()
        for model in self.authoring_models:
            for instance in model.objects.all():
                if instance.first_published_at is not None:
                    if hasattr(instance, "authors"):
                        for author in instance.authors.stream_data:
                            if author['value'] == self.id:
                                contributions_set.add(instance)
                    if hasattr(instance, "presenters"):
                        for presenter in instance.presenters.stream_data:
                            if presenter['value'] == self.id:
                                contributions_set.add(instance)
        
        contributions = list(contributions_set)
        # Order the contributions list by instance.first_published_at
        if contributions:
            contributions.sort(key=attrgetter("first_published_at"), reverse=True)

        return contributions


@register_setting(icon="group")
class PeoplePagesSettings(BaseSetting):
    live_profiles = StreamField([
        ('contributor', ModelChooserBlock('people.Contributor')),
    ], blank=True)
    title = models.CharField(blank=True, max_length=500)
    description = RichTextField(null=True, blank=True, help_text="Description/caption to display at top of list page")

    panels = [
        FieldPanel('title'),
        FieldPanel('description'),
        StreamFieldPanel('live_profiles')
    ]

@hooks.register('register_admin_menu_item')
def register_peoplepages_menu_item():
    '''
    Generate a second menu button at the root of the Admin sidebar
    '''
    return SettingMenuItem(PeoplePagesSettings, icon="group")

@register_snippet
class Certification(models.Model):
    cert_name = models.CharField(max_length=63, help_text='Name of the Certification')
    badge_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text='Badge associated with this certification'
    )

    panels = [
        FieldPanel('cert_name'),
        ImageChooserPanel('badge_image')
    ]

    def __str__(self):
        return self.cert_name

class PeopleCertification(models.Model):
    person = ParentalKey('people.PeoplePageModel', related_name='certifications', on_delete=models.CASCADE)
    certification = models.ForeignKey('people.Certification', related_name='certified_people', on_delete=models.CASCADE)
    date_certified = models.DateField(help_text='The most recent date you were certified', null=True, blank=True)
    badge_url = models.URLField(help_text='The badge URL provided by the certifying organization', null=True, blank=True)

    panels = [
        SnippetChooserPanel('certification'),
        FieldPanel('date_certified'),
        FieldPanel('badge_url')
    ]

    class Meta:
        unique_together = ('person', 'certification')


class PeoplePageSkill(models.Model):
    people_page = ParentalKey('people.PeoplePageModel', related_name='skills', on_delete=models.CASCADE)
    text = models.CharField(max_length=63, help_text='Description of Skill')

class PeoplePageInterest(models.Model):
    people_page = ParentalKey('people.PeoplePageModel', related_name='interests', on_delete=models.CASCADE)
    text = models.CharField(max_length=63, help_text='Description of Interest')

@register_model_chooser
class PeoplePageModel(ClusterableModel):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        related_name='people_page_model',
        on_delete=models.CASCADE,
        primary_key=True
    )

    role_at_excella = models.CharField(max_length=63, help_text='Your Role or Job Title', null=True, blank=True)
    area_of_expertise = models.CharField(max_length=127, help_text='Your Area of Expertise', null=True, blank=True)

    headline = models.CharField(max_length=63, help_text='Headline about yourself that appears under your name', null=True, blank=True)

    facebook = models.URLField(help_text='Your Facebook page URL', null=True, blank=True)
    twitter = models.CharField(max_length=255, help_text='Your twitter username, without the @', null=True, blank=True)
    linkedin = models.URLField(help_text='Your LinkedIn page URL', null=True, blank=True)
    instagram = models.CharField(max_length=255, help_text='Your instagram username', null=True, blank=True)
    github = models.CharField(max_length=255, help_text='Your GitHub username', null=True, blank=True)

    summary = RichTextField(help_text='A summary about yourself, 2-3 paragraphs', default=' ')

    panels = [
        FieldPanel('role_at_excella'),
        FieldPanel('area_of_expertise'),
        FieldPanel('headline'),
        FieldPanel('summary'),
        InlinePanel('skills', label='Skills'),
        InlinePanel('interests', label='Interests'),
        InlinePanel('certifications', label='Certifications'),
        FieldPanel('facebook'),
        FieldPanel('twitter'),
        FieldPanel('linkedin'),
        FieldPanel('instagram'),
        FieldPanel('github')
    ]

    def __str__(self):
        return 'Profile Page ' + self.user.first_name + ' ' + self.user.last_name

class PeoplePageModelAdmin(ModelAdmin):
    model = PeoplePageModel
    edit_view_class = PeoplePageEditView

modeladmin_register(PeoplePageModelAdmin)

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_person_page_model(sender, created, instance, **kwargs):
    if created:
        personPageModel = PeoplePageModel(user=instance)
        personPageModel.save()

        content_type = ContentType.objects.get_for_model(PeoplePageModel)
        permission = Permission.objects.create(
            codename='can_edit_' + instance.username,
            name='Can edit ' + instance.username,
            content_type=content_type,
        )

        instance.user_permissions.add(permission)

@hooks.register('register_account_menu_item')
def register_profile_menu_item(request):
    person_page_id = PeoplePageModel.objects.get(user=request.user).pk
    edit_url =  PeoplePageModelAdmin().url_helper.get_action_url('edit', person_page_id)

    return {
        'url': edit_url,
        'label': 'Profile',
    }

@hooks.register('construct_main_menu')
def hide_peoplepagemodeladmin_menu_item(request, menu_items):
    menu_items[:] = [item for item in menu_items if item.name != 'people-page-models']
