from django.urls import include, path
from django.conf.urls import url
from .views import PeopleListView, PeopleDetailView

urlpatterns = [
    path(r'', PeopleListView.as_view(), name='people_list'),
    url(r'^(?P<slug>[\w-]+)/$', PeopleDetailView.as_view(), name='people_detail'),
]