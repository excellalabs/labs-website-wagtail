from django.apps import AppConfig


class HomeConfig(AppConfig):
    name = 'home'

    def ready(self):
        from people.models import Contributor
        # print("+++++++++++++++++++++++++++++++ READY +++++++")
        Contributor.find_contributed_models()
        