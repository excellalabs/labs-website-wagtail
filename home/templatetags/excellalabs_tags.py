from django import template
from django.conf import settings
from django.template import Library, TemplateSyntaxError
from django.template.defaulttags import (CommentNode, IfNode, LoadNode, find_library, load_from_library)
from django.template.smartif import Literal

try:
    from django.template.base import TokenType
    TOKEN_BLOCK = TokenType.BLOCK
except ImportError:
    from django.template.base import TOKEN_BLOCK

register = template.Library()

@register.simple_tag
def admin_enabled():
    return settings.ENABLE_ADMIN

@register.tag
def load_admin(parser, token):
    if settings.ENABLE_ADMIN:
        lib = find_library(parser, 'wagtailuserbar')
        parser.add_library(lib)
    else:
        pass
    return LoadNode()

@register.tag
def if_admin(parser, token):
    end_tag = 'end%s' % list(token.split_contents())[0]
    nodelist_true = nodelist_false = CommentNode()
    if settings.ENABLE_ADMIN:
        nodelist_true = parser.parse(('else', end_tag))
        token = parser.next_token()
        if token.contents == 'else':
            parser.skip_past(end_tag)
    else:
        while parser.tokens:
            token = parser.next_token()
            if token.token_type == TOKEN_BLOCK and token.contents == end_tag:
                return IfNode([
                    (Literal(settings.ENABLE_ADMIN), nodelist_true),
                    (None, nodelist_false)
                ])
            elif token.token_type == TOKEN_BLOCK and token.contents == 'else':
                break
        nodelist_false = parser.parse((end_tag,))
        parser.next_token()
    return IfNode([(Literal(settings.ENABLE_ADMIN), nodelist_true), (None, nodelist_false)])
