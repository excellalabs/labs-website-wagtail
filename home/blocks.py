from django.db import models

from wagtail.core import blocks
import feedparser

from wagtail.images.blocks import ImageChooserBlock
from wagtail.core.fields import StreamField
from wagtailcodeblock.blocks import CodeBlock


class FAIconLinkBlock(blocks.StructBlock):
    icon = blocks.CharBlock(help_text="Font Awesome icon name")
    url = blocks.URLBlock(help_text="URL to link to")
    class Meta:
        icon = "link"
        template = "home/blocks/icon_link.html"


class DetailBlock(blocks.StructBlock):
    title = blocks.CharBlock(required=False)
    text = blocks.RichTextBlock(blank=True)

    class Meta:
        icon = "user"
        template = "home/blocks/detail.html"


class PullQuoteBlock(blocks.BlockQuoteBlock):
    class Meta:
        template='home/blocks/pull_quote_block.html'

class GalleryBlock(blocks.ListBlock):
    class Meta:
        template='home/blocks/gallery_block.html'
        icon='image'

class ParagraphBlock(blocks.StructBlock):
    text= blocks.RichTextBlock()
    image = ImageChooserBlock(required=False)
    class Meta:
        template='home/blocks/paragraph_block.html'
        icon='pilcrow'

class ContentHighlightBlock(blocks.StructBlock):
    text= blocks.RichTextBlock()
    image = ImageChooserBlock(required=False)
    class Meta:
        template='home/blocks/content_highlight_block.html'
        icon='tag'

class CodeExcerptBlock(blocks.StructBlock):
    code = CodeBlock(label='Code')

    class Meta:
        template='home/blocks/code_excerpt_block.html'
        icon='code'