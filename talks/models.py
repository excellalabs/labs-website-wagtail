from django.db import models

# Create your models here.
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel, MultiFieldPanel, PageChooserPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.core.fields import RichTextField, StreamField
from wagtail.core.models import Page
from home.blocks import ParagraphBlock, PullQuoteBlock, GalleryBlock, ContentHighlightBlock, CodeExcerptBlock
from wagtail.images.blocks import ImageChooserBlock
from wagtail.core.blocks import CharBlock
from django.db.models import Q

from wagtailmodelchooser.blocks import ModelChooserBlock

from home.models import BasePage

class VideoList(Page):
    subpage_types = ['talks.Video', 'talks.Article']
    copy = RichTextField(null=True, blank=True, help_text="Summary to display at top of list page")
    featured_video = models.ForeignKey(
        'talks.Video',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text="Video featured at top of list page"
    )

    content_panels = Page.content_panels + [
        FieldPanel('copy'),
        PageChooserPanel('featured_video'),
    ]

    def get_context(self, request):
        context = super(VideoList, self).get_context(request)
        context['ordered_children'] = self.get_children().live().specific().order_by('video__date')
        if self.featured_video:
            context['ordered_children'] = context['ordered_children'].filter(~Q(id = self.featured_video.pk))
        return context


TAGS_CHOICES = (
    ("Agile", "Agile"),
    ("Cloud Engineering", "Cloud Engineering"),
    ("DevOps", "DevOps"),
    ("DevOps Automation", "DevOps Automation"),
    ("Digital Strategy", "Digital Strategy"),
    ("JavaScript", "JavaScript"),
    (".NET", ".NET"),
    ("Python", "Python"),
    ('Professional Development', "Professional Development"),
    ("Software Development", "Software Development"),
)

class Article(BasePage):

    subtitle = models.CharField(blank=True, max_length=500)
    author = models.CharField(max_length=200)

    authors = StreamField([
        ('contributor', ModelChooserBlock('people.Contributor')),
        ('custom_name', CharBlock(max_length=200))
    ], blank=True)

    date = models.DateField(null=True, blank=True)
    tags = models.CharField(choices=TAGS_CHOICES, max_length=100, null=True, blank=True)

    body = StreamField([
            ('paragraph_block', ParagraphBlock()),
            ('pull_quote_block', PullQuoteBlock()),
            ('gallery_block', GalleryBlock(ImageChooserBlock())),
            ('content_highlight_block', ContentHighlightBlock()),
            ('code_excerpt_block', CodeExcerptBlock()),
        ], blank=True)

    content_panels = BasePage.content_panels + [
        FieldPanel('subtitle'),
        FieldPanel('author', classname='full'),
        StreamFieldPanel('authors', classname='full'),
        FieldPanel('date', classname='full'),
        FieldPanel('tags', classname='full'),
        StreamFieldPanel('body', classname='full'),
        MultiFieldPanel(
            [
                FieldPanel('summary'),
                ImageChooserPanel('listing_image'),
            ],
            heading="Listing Page content"
        ),
    ]

    parent_page_types = ['talks.VideoList']

class Video(BasePage):
    
    author = models.CharField(max_length=200)

    authors = StreamField([
        ('contributor', ModelChooserBlock('people.Contributor')),
        ('custom_name', CharBlock(max_length=200))
    ], blank=True)

    video_url = models.CharField(max_length=200, null=True, blank=True)
    date = models.DateField(null=True, blank=True)
    tags = models.CharField(choices=TAGS_CHOICES, max_length=100, null=True, blank=True)

    content_panels = BasePage.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel('description'),
            ],
            heading="Hero Banner Content",
        ),
        FieldPanel('author', classname='full'),
        StreamFieldPanel('authors', classname='full'),
        FieldPanel('video_url', classname='full'),
        FieldPanel('date', classname='full'),
        FieldPanel('tags', classname='full'),
        MultiFieldPanel(
            [
                FieldPanel('summary'),
                ImageChooserPanel('listing_image'),
            ],
            heading="Listing Page content"
        ),
    ]

    parent_page_types = ['talks.VideoList']
