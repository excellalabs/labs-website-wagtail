# Generated by Django 2.1.4 on 2019-05-03 14:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('talks', '0024_auto_20190405_1344'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='tags',
            field=models.CharField(blank=True, choices=[('Agile', 'Agile'), ('Cloud Engineering', 'Cloud Engineering'), ('DevOps', 'DevOps'), ('DevOps Automation', 'DevOps Automation'), ('Digital Strategy', 'Digital Strategy'), ('JavaScript', 'JavaScript'), ('.NET', '.NET'), ('Python', 'Python'), ('Professional Development', 'Professional Development'), ('Software Development', 'Software Development')], max_length=100, null=True),
        ),
    ]
