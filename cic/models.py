from django.db import models

# Create your models here.
from wagtail.core import hooks
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel, MultiFieldPanel
from wagtail.core.fields import RichTextField, StreamField
from wagtail.core.models import Page
from wagtail.core.blocks import RichTextBlock
from wagtail.images.blocks import ImageChooserBlock
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.contrib.table_block.blocks import TableBlock
from wagtail.contrib.settings.models import BaseSetting, register_setting
from wagtail.contrib.settings.registry import SettingMenuItem

from .blocks import IFrameBlock

from home.models import BasePage
from home.blocks import ParagraphBlock, PullQuoteBlock, GalleryBlock, ContentHighlightBlock, CodeExcerptBlock


class CicList(Page):
    subpage_types = ['cic.CicPage']
    sub_title = RichTextField(null=True, blank=True, help_text="Summary to display at top of list page")
    details_top = StreamField([
        ('paragraph_block', ParagraphBlock()),
        ('pull_quote_block', PullQuoteBlock()),
        ('gallery_block', GalleryBlock(ImageChooserBlock())),
        ('content_highlight_block', ContentHighlightBlock()),
        ('code_excerpt_block', CodeExcerptBlock()),
        ('table_block', TableBlock(template='home/blocks/table_block.html')),
        ('iframe_block', IFrameBlock())
    ], blank=True, help_text='Content in top part of the page, above the categories.')
    details_bottom = StreamField([
        ('paragraph_block', ParagraphBlock()),
        ('pull_quote_block', PullQuoteBlock()),
        ('gallery_block', GalleryBlock(ImageChooserBlock())),
        ('content_highlight_block', ContentHighlightBlock()),
        ('code_excerpt_block', CodeExcerptBlock()),
        ('table_block', TableBlock(template='home/blocks/table_block.html')),
        ('iframe_block', IFrameBlock())
    ], blank=True, help_text='Content in bottom part of the page, bellow the categories.')

    content_panels = Page.content_panels + [
        MultiFieldPanel([
            FieldPanel('sub_title'),
            StreamFieldPanel('details_top'),
            StreamFieldPanel('details_bottom')
        ])
    ]


class CicPage(BasePage):
    summary = None
    icon = models.CharField(max_length=255, default='fa-table', null=False, blank=False,
                            help_text='Category Icon. See https://fontawesome.com/v4.7.0/icons/')
    body = StreamField([
        ('paragraph_block', ParagraphBlock()),
        ('pull_quote_block', PullQuoteBlock()),
        ('gallery_block', GalleryBlock(ImageChooserBlock())),
        ('content_highlight_block', ContentHighlightBlock()),
        ('code_excerpt_block', CodeExcerptBlock()),
        ('table_block', TableBlock(template='home/blocks/table_block.html')),
        ('iframe_block', IFrameBlock())
    ], blank=True)

    content_panels = BasePage.content_panels + [
        FieldPanel('icon'),
        MultiFieldPanel([
            FieldPanel('description'),
            ImageChooserPanel('header_image'),
        ],
            heading="Hero Banner Content",
        ),
        StreamFieldPanel('body', classname='full'),
    ]

    parent_page_types = ['cic.CicList']


@register_setting(icon="grip")
class CICSettings(BaseSetting):
    class Meta:
        verbose_name = 'CIC Project Settings'

    disclaimer_font_size = models.FloatField(null=False, blank=False, help_text='CSS rem size for Disclaimer Text',
                                             default=1.0)
    disclaimer = RichTextField(null=False, blank=False, help_text="Legal Disclaimer",
                               default='<p>This is a disclaimer</p><p></p><p>We are not responsible.</p>')
    padding_bottom_size = models.FloatField(null=False, blank=False, default=1.0,
                                            help_text='CSS rem Padding on bottom of blocks for CIC pages.')

    panels = [
        MultiFieldPanel([
            FieldPanel('padding_bottom_size'),
        ], heading='Page Styling'),
        MultiFieldPanel([
            FieldPanel('disclaimer_font_size'),
            FieldPanel('disclaimer')
        ], heading='Disclaimer Settings')
    ]


@hooks.register('register_admin_menu_item')
def register_cic_project_settings_menu_item():
    """
    Add the CIC Project Settings to the main Admin Sidebar
    """
    return SettingMenuItem(CICSettings, icon='grip')
