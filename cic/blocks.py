from django.db import models

from wagtail.core import blocks
from wagtail.images.blocks import ImageChooserBlock
from wagtail.contrib.table_block.blocks import TableBlock

from home.blocks import ParagraphBlock, PullQuoteBlock, GalleryBlock, ContentHighlightBlock, CodeExcerptBlock


class CardBlock(blocks.StructBlock):
    text_alignment = blocks.ChoiceBlock([
        ('', 'Left'),
        ('text-center', 'Center'),
        ('text-right', 'Right')
    ], required=False)
    header = blocks.CharBlock(help_text='Card Header', required=False)
    icon = blocks.CharBlock(help_text='Font Awesome icon name', required=False)
    icon_size = blocks.ChoiceBlock([
        ('', 'Normal'),
        ('fa-lg', 'Large'),
        ('fa-2x', '2x'),
        ('fa-3x', '3x'),
        ('fa-4x', '4x'),
        ('fa-5x', '5x')
    ], required=False, help_text='Font Awesome Icon Size')
    title = blocks.CharBlock(help_text='Card Title', required=False)
    title_size = blocks.IntegerBlock(min_value=1, max_value=6, default=5, help_text='Card Title Header Size')
    body = blocks.StreamBlock(local_blocks=[
        ('paragraph_block', ParagraphBlock()),
        ('pull_quote_block', PullQuoteBlock()),
        ('gallery_block', GalleryBlock(ImageChooserBlock())),
        ('content_highlight_block', ContentHighlightBlock()),
        ('code_excerpt_block', CodeExcerptBlock()),
        ('table_block', TableBlock(template='home/blocks/table_block.html')),
    ], required=False)
    footer = blocks.CharBlock(help_text='Card Footer', required=False)

    class Meta:
        template = 'cic/blocks/card_block.html'


class CardColumnsBlock(blocks.StructBlock):

    cards = blocks.StreamBlock(local_blocks=[
        ('card_block', CardBlock())
    ])

    class Meta:
        template = 'cic/blocks/card_columns.html'


class IFrameBlock(blocks.StructBlock):
    url = blocks.CharBlock(required=True, help_text='URL of iFrame to embed.')
    height = blocks.FloatBlock(required=True, help_text='CSS em height size', default=50.0)

    class Meta:
        template = 'cic/blocks/iframe_block.html'
