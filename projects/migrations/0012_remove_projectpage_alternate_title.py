# Generated by Django 2.1.4 on 2019-04-03 20:11

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0011_projectlist_copy'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='projectpage',
            name='alternate_title',
        ),
    ]
