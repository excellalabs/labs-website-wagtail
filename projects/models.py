from django.db import models

# Create your models here.
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel, MultiFieldPanel
from wagtail.core.fields import RichTextField, StreamField
from wagtail.core.models import Page
from wagtail.core.blocks import RichTextBlock
from wagtail.images.blocks import ImageChooserBlock
from wagtail.images.edit_handlers import ImageChooserPanel

from home.models import BasePage
from home.blocks import ParagraphBlock, PullQuoteBlock, GalleryBlock, ContentHighlightBlock, CodeExcerptBlock


class ProjectList(Page):
    subpage_types = ['projects.ProjectPage']
    copy = RichTextField(null=True, blank=True, help_text="Summary to display at top of list page")

    content_panels = Page.content_panels + [
        FieldPanel('copy')
    ]

class ProjectPage(BasePage):

    body = StreamField([
        ('paragraph_block', ParagraphBlock()),
        ('pull_quote_block', PullQuoteBlock()),
        ('gallery_block', GalleryBlock(ImageChooserBlock())),
        ('content_highlight_block', ContentHighlightBlock()),
        ('code_excerpt_block', CodeExcerptBlock()),
    ], blank=True)

    content_panels = BasePage.content_panels + [
        MultiFieldPanel([
            FieldPanel('description'),
            ImageChooserPanel('header_image'),
        ],
        heading="Hero Banner Content",
        ),
        StreamFieldPanel('body', classname='full'),
        MultiFieldPanel(
            [
                FieldPanel('summary'),
                ImageChooserPanel('listing_image'),
            ],
            heading="Listing Page content"
        ),        
    ]

    parent_page_types = ['projects.ProjectList']
