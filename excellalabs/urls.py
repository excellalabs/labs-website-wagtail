from django.conf import settings
from django.conf.urls import include, url
from django.urls import path
from django.contrib import admin

from wagtail.admin import urls as wagtailadmin_urls
from wagtail.core import urls as wagtail_urls
from wagtail.documents import urls as wagtaildocs_urls
from search import views as search_views
from .views import DraftsView
import django_saml2_auth.views


urlpatterns = [
    url(r'^people/', include('people.urls')),
    url(r'^documents/', include(wagtaildocs_urls)),
    url(r'^search/$', search_views.search, name='search'),

    # For anything not caught by a more specific rule above, hand over to
    # Wagtail's page serving mechanism. This should be the last pattern in
    # the list:
    url(r'', include(wagtail_urls)),

    # Alternatively, if you want Wagtail pages to be served from a subpath
    # of your site, rather than the site root:
    #    url(r'^pages/', include(wagtail_urls)),
]

if settings.ENABLE_ADMIN:
    urlpatterns.insert(0, url(r'^drafts/$', DraftsView.as_view(), name='drafts'))
    urlpatterns.insert(0, url(r'^django-admin/', admin.site.urls))
    urlpatterns.insert(0, url(r'^admin/', include(wagtailadmin_urls)))

if settings.SAML2_AUTH:
    # These are the SAML2 related URLs. You can change "^saml2_auth/" regex to
    # any path you want, like "^sso_auth/", "^sso_login/", etc. (required)
    urlpatterns.insert(0, url(r'^saml2_auth/', include('django_saml2_auth.urls')))


    # The following line will replace the default user login with SAML2 (optional)
    # If you want to specific the after-login-redirect-URL, use parameter "?next=/the/path/you/want"
    # with this view.
    urlpatterns.insert(0, url(r'^accounts/login/$', django_saml2_auth.views.signin))

    # The following line will replace the admin login with SAML2 (optional)
    # If you want to specific the after-login-redirect-URL, use parameter "?next=/the/path/you/want"
    # with this view.
    urlpatterns.insert(0, url(r'^admin/login/$', django_saml2_auth.views.signin))

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
