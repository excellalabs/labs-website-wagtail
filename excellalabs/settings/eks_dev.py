from .base import *

DEBUG = False

SECRET_KEY = os.getenv('SECRET_KEY')
AWS_STORAGE_BUCKET_NAME = os.getenv('AWS_STORAGE_BUCKET_NAME')
AWS_S3_CUSTOM_DOMAIN = '{}.s3.amazonaws.com'.format(AWS_STORAGE_BUCKET_NAME)
ALLOWED_HOSTS = ['*']
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

MEDIA_URL = 'https://{}/'.format(AWS_S3_CUSTOM_DOMAIN)
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'

#email settings
EMAIL_BACKEND = 'django_ses.SESBackend'
WAGTAILADMIN_NOTIFICATION_FROM_EMAIL = 'admin@excellalabs.com'
BASE_URL = 'http://dev-excellalabs.com'

# Google Analytics
GOOGLE_ANALYTICS_TRACKING_ID = 'UA-161003532-2'