from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '8n)huzsw288jgey-i=r6_t5dbqu_6ip6z@=#3t@h*8x(j@n4*p'
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# Google Analytics
GOOGLE_ANALYTICS_TRACKING_ID = 'UA-1610035-FAKE'