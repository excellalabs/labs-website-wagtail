from .base import *

DEBUG = False

SECRET_KEY = os.getenv('SECRET_KEY')
AWS_STORAGE_BUCKET_NAME = os.getenv('AWS_STORAGE_BUCKET_NAME')
AWS_S3_CUSTOM_DOMAIN = '{}.s3.amazonaws.com'.format(AWS_STORAGE_BUCKET_NAME)
ALLOWED_HOSTS = ['excellalabscontent.com', 'excellalabs.com', '*']

MEDIA_URL = 'https://{}/'.format(AWS_S3_CUSTOM_DOMAIN)
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'

#email settings
EMAIL_BACKEND = 'django_ses.SESBackend'
WAGTAILADMIN_NOTIFICATION_FROM_EMAIL = 'admin@excellalabs.com'
BASE_URL = 'https://excellalabscontent.com'
AWS_SES_REGION_NAME = 'us-west-2'
AWS_SES_REGION_ENDPOINT = 'email.us-west-2.amazonaws.com'

#SAML2 configuration
SAML2_AUTH = {
    # Metadata is required, choose either remote url or local file path
    'METADATA_LOCAL_FILE_PATH': os.path.join(PROJECT_DIR, 'metadata/JumpCloud-saml2-metadata.xml'),
    'DEFAULT_NEXT_URL': '/admin/',
    'CREATE_USER': 'TRUE',
    'NEW_USER_PROFILE': {
        'USER_GROUPS': ['Editors'],
    },
    # Optional settings below
    'ATTRIBUTES_MAP': {  # Change Email/UserName/FirstName/LastName to corresponding SAML2 userprofile attributes.                                                                                                 
        'email': 'Email',                                                                                                                                                                                          
        'username': 'UserName',                                                                                                                                                                                    
        'first_name': 'FirstName',                                                                                                                                                                                 
        'last_name': 'LastName',                                                                                                                                                                                   
    }, 
    'ASSERTION_URL': 'https://excellalabscontent.com', # Custom URL to validate incoming SAML requests against
    'ENTITY_ID': 'https://www.excella.com', # Populates the Issuer element in authn request
}

# Feature Flags
ENABLE_PEOPLE_LIST = False

# Google Analytics
GOOGLE_ANALYTICS_TRACKING_ID = 'UA-161003532-1'
