# some_app/views.py
from django.views.generic import TemplateView
from wagtail.core.models import Page
from django.db.models import Q
from django.contrib.auth.mixins import UserPassesTestMixin
from django.urls import reverse
from wagtail.core import hooks
from wagtail.admin.menu import MenuItem
from wagtail.contrib.settings.registry import SettingMenuItem

class DraftsView(UserPassesTestMixin, TemplateView):
    template_name = "drafts.html"

    def test_func(self):
        return is_moderator_or_admin(self.request)

    def get_context_data(self, **kwargs):
        context = super(DraftsView, self).get_context_data(**kwargs)
        drafts = Page.objects.filter(Q(live = False) | Q(has_unpublished_changes = True)).specific()

        for draft in drafts:
            if draft.has_unpublished_changes:
                if draft.live:
                    draft.status = "Published Page and Draft"
                else:
                    draft.status = "Unpublished Draft"

        context['drafts'] = drafts
        return context

class PublisherOrSuperUserAdminMenuItem(MenuItem):
    def is_shown(self, request):
        return is_moderator_or_admin(request)

def is_moderator_or_admin(request):
    if request.user.is_superuser:
        return True
    else:
        return request.user.groups.filter(name = 'Moderators').exists()


@hooks.register('register_admin_menu_item')
def register_drafts_menu_item():
    return PublisherOrSuperUserAdminMenuItem('Drafts', reverse('drafts'), classnames='icon icon-doc-empty-inverse', order=300)

