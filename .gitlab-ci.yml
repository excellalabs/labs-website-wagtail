stages:
  - build
  - tag
  - scan
  - test
  - deploy

services:
  - docker:18-dind

before_script:
  - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY

build:
  image: docker:stable
  stage: build
  script:
    - docker build --pull -t $CI_REGISTRY/excellalabs/labs-website-wagtail:${CI_COMMIT_SHA:0:8} .
    - docker push $CI_REGISTRY/excellalabs/labs-website-wagtail:${CI_COMMIT_SHA:0:8}

tag:
  stage: tag
  image: docker:stable
  script:
    - docker pull $CI_REGISTRY/excellalabs/labs-website-wagtail:${CI_COMMIT_SHA:0:8}
    - docker tag $CI_REGISTRY/excellalabs/labs-website-wagtail:${CI_COMMIT_SHA:0:8} $CI_REGISTRY/excellalabs/labs-website-wagtail:latest
    - docker push $CI_REGISTRY/excellalabs/labs-website-wagtail:${CI_COMMIT_SHA:0:8}
  only: 
    - master

container_scanning:
  image: docker:stable
  allow_failure: true
  variables:
    DOCKER_DRIVER: overlay2
  stage: scan
  services:
    - docker:stable-dind
  script:
    - docker run -d --name db arminc/clair-db:latest
    - docker run -p 6060:6060 --link db:postgres -d --name clair --restart on-failure arminc/clair-local-scan:v2.0.1
    - apk add -U wget ca-certificates
    - docker pull $CI_REGISTRY/excellalabs/labs-website-wagtail:${CI_COMMIT_SHA:0:8}
    - wget https://github.com/arminc/clair-scanner/releases/download/v8/clair-scanner_linux_amd64
    - mv clair-scanner_linux_amd64 clair-scanner
    - chmod +x clair-scanner
    - touch clair-whitelist.yml
    - while( ! wget -q -O /dev/null http://docker:6060/v1/namespaces ) ; do sleep 1 ; done
    - retries=0
    - echo "Waiting for clair daemon to start"
    - while( ! wget -T 10 -q -O /dev/null http://docker:6060/v1/namespaces ) ; do sleep 1 ; echo -n "." ; if [ $retries -eq 10 ] ; then echo " Timeout, aborting." ; exit 1 ; fi ; retries=$(($retries+1)) ; done
    - ./clair-scanner -c http://docker:6060 --ip $(hostname -i) -r gl-container-scanning-report.json -l clair.log -w clair-whitelist.yml $CI_REGISTRY/excellalabs/labs-website-wagtail:${CI_COMMIT_SHA:0:8} || true
  artifacts:
    reports:
      container_scanning: gl-container-scanning-report.json
  only:
    - master

test:
  image: docker:stable
  stage: test
  script:
    - docker pull $CI_REGISTRY/excellalabs/labs-website-wagtail:${CI_COMMIT_SHA:0:8}
    - docker run $CI_REGISTRY/excellalabs/labs-website-wagtail:${CI_COMMIT_SHA:0:8} python manage.py test

dev_environment_deploy:
  image: $CI_REGISTRY/excellalabs/docker-aws-kubectl-cli:latest
  stage: deploy
  environment:
    name: dev
    url: http://dev-excellalabs.com
  script:
    - aws eks update-kubeconfig --name $DEV_K8S_CLUSTER --region us-east-1
    - kubectl apply -f deployment/namespace.yaml
    - kubectl apply -f deployment/config-map-dev.yaml
    - sed -i -e "s/{{COMMIT}}/${CI_COMMIT_SHA:0:8}/g" deployment/deployment-admin.yaml
    - sed -i -e "s/{{REPLICAS}}/1/g" deployment/deployment-admin.yaml
    - sed -i -e "s/{{GA_VIEW_ID}}/$GA_VIEW_ID/g" deployment/secrets.yaml
    - GA_KEY_CONTENT_ENCODED="$(base64 $GA_KEY_CONTENT | tr -d '\n')"
    - sed -i -e "s/{{GA_KEY_CONTENT_ENCODED}}/$GA_KEY_CONTENT_ENCODED/g" deployment/secrets.yaml
    - DJANGO_SECRET_KEY_ENCODED="$(base64 $DJANGO_SECRET_KEY | tr -d '\n')"
    - sed -i -e "s/{{DJANGO_SECRET_KEY_ENCODED}}/$DJANGO_SECRET_KEY_ENCODED/g" deployment/secrets.yaml
    - DATABASE_URL_ENCODED="$(base64 $LABS_DATABASE_URL | tr -d '\n')"
    - sed -i -e "s/{{DATABASE_URL_ENCODED}}/$DATABASE_URL_ENCODED/g" deployment/secrets.yaml
    - kubectl apply -f deployment/secrets.yaml
    - kubectl apply -f deployment/deployment-admin.yaml
    - kubectl apply -f deployment/service-admin-dev.yaml
    - kubectl apply -f deployment/ingress-dev.yaml
    - kubectl rollout status deployments excellalabs-admin-deployment --namespace excellalabs -w
  only:
    - master

prod_environment_deploy:
  image: $CI_REGISTRY/excellalabs/docker-aws-kubectl-cli:latest
  stage: deploy
  environment: 
    name: production
    url: https://excellalabs.com
  when: manual
  script:
    #non admin site
    - kubectl --token=$PROD_K8S_TOKEN -s $PROD_K8S_URL --insecure-skip-tls-verify=true apply -f deployment/namespace.yaml
    - kubectl --token=$PROD_K8S_TOKEN -s $PROD_K8S_URL --insecure-skip-tls-verify=true apply -f deployment/config-map-prod.yaml
    - sed -i -e "s/{{COMMIT}}/${CI_COMMIT_SHA:0:8}/g" deployment/deployment.yaml
    - sed -i -e "s/{{REPLICAS}}/3/g" deployment/deployment.yaml
    - kubectl --token=$PROD_K8S_TOKEN -s $PROD_K8S_URL --insecure-skip-tls-verify=true apply -f deployment/deployment.yaml
    - kubectl --token=$PROD_K8S_TOKEN -s $PROD_K8S_URL --insecure-skip-tls-verify=true apply -f deployment/service.yaml
    - kubectl --token=$PROD_K8S_TOKEN -s $PROD_K8S_URL --insecure-skip-tls-verify=true apply -f deployment/ingress.yaml
    - kubectl --token=$PROD_K8S_TOKEN -s $PROD_K8S_URL --insecure-skip-tls-verify=true rollout status deployments excellalabs-deployment --namespace excellalabs -w
    #admin site
    - sed -i -e "s/{{COMMIT}}/${CI_COMMIT_SHA:0:8}/g" deployment/deployment-admin.yaml
    - sed -i -e "s/{{REPLICAS}}/3/g" deployment/deployment-admin.yaml
    - sed -i -e "s/{{GA_VIEW_ID}}/$GA_VIEW_ID/g" deployment/secrets.yaml
    - GA_KEY_CONTENT_ENCODED="$(base64 $GA_KEY_CONTENT | tr -d '\n')"
    - sed -i -e "s/{{GA_KEY_CONTENT_ENCODED}}/$GA_KEY_CONTENT_ENCODED/g" deployment/secrets.yaml
    - DJANGO_SECRET_KEY_ENCODED="$(base64 $DJANGO_SECRET_KEY | tr -d '\n')"
    - sed -i -e "s/{{DJANGO_SECRET_KEY_ENCODED}}/$DJANGO_SECRET_KEY_ENCODED/g" deployment/secrets.yaml
    - DATABASE_URL_ENCODED="$(base64 $LABS_DATABASE_URL | tr -d '\n')"
    - sed -i -e "s/{{DATABASE_URL_ENCODED}}/$DATABASE_URL_ENCODED/g" deployment/secrets.yaml    
    - kubectl --token=$PROD_K8S_TOKEN -s $PROD_K8S_URL --insecure-skip-tls-verify=true apply -f deployment/secrets.yaml
    - kubectl --token=$PROD_K8S_TOKEN -s $PROD_K8S_URL --insecure-skip-tls-verify=true apply -f deployment/deployment-admin.yaml
    - kubectl --token=$PROD_K8S_TOKEN -s $PROD_K8S_URL --insecure-skip-tls-verify=true apply -f deployment/service-admin.yaml
    - kubectl --token=$PROD_K8S_TOKEN -s $PROD_K8S_URL --insecure-skip-tls-verify=true apply -f deployment/ingress-admin.yaml
    - kubectl --token=$PROD_K8S_TOKEN -s $PROD_K8S_URL --insecure-skip-tls-verify=true rollout status deployments excellalabs-admin-deployment --namespace excellalabs -w
  only:
    - master