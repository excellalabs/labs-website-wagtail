from django.db import models
from wagtail.core.models import Page, Orderable
from modelcluster.models import ClusterableModel
from wagtail.core.fields import RichTextField, StreamField
from wagtail.core import blocks
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel, MultiFieldPanel, InlinePanel, PageChooserPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.images.blocks import ImageChooserBlock
from wagtail.core import blocks
from modelcluster.fields import ParentalKey
from home.blocks import ParagraphBlock, PullQuoteBlock, GalleryBlock, ContentHighlightBlock
from wagtail.core.blocks import CharBlock
from wagtailmodelchooser.blocks import ModelChooserBlock
from django.db.models import Q
from home.models import BasePage


class EventList(Page):
    subpage_types = ['events.Event']
    copy = RichTextField(null=True, blank=True, help_text="Summary to display at top of list page")
    featured_event = models.ForeignKey(
        'events.Event',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text="Event featured at top of list page"
    )

    content_panels = Page.content_panels + [
        FieldPanel('copy'),
        PageChooserPanel('featured_event'),
    ]

    def get_context(self, request):
        context = super(EventList, self).get_context(request)
        context['ordered_children'] = self.get_children().live().specific().order_by('event__start_date')
        if self.featured_event:
            context['ordered_children'] = context['ordered_children'].filter(~Q(id = self.featured_event.pk))
        return context


class Event(BasePage):
    start_date = models.DateField("Event start date")
    end_date = models.DateField("Event end date", null=True, blank=True)
    location = models.CharField(max_length=100, null=True, blank=True)
    city = models.CharField(max_length=100, null=True, blank=True)
    state_code = models.CharField(max_length=2, null=True, blank=True)
    address = RichTextField(help_text='', null=True, blank=True)
    website_link = models.TextField(help_text='', null=True, blank=True)
    registration_link = models.TextField(help_text='', null=True, blank=True)

    content_panels = BasePage.content_panels + [
        MultiFieldPanel(
            [
                FieldPanel('description'),
                ImageChooserPanel('header_image'),
            ],
            heading="Hero Banner Content",
        ),
        FieldPanel('start_date', classname='full'),
        FieldPanel('end_date', classname='full'),
        FieldPanel('location', classname='full'),
        FieldPanel('city', classname='full'),
        FieldPanel('state_code', classname='full'),
        FieldPanel('address', classname='full'),
        FieldPanel('website_link', classname='full'),
        FieldPanel('registration_link', classname='full'),
        MultiFieldPanel(
            [
                FieldPanel('summary'),
                ImageChooserPanel('listing_image'),
            ],
            heading="Listing Page content"
        ),
        InlinePanel('event_presentations', label="Event Presentations"),
    ]

    parent_page_types = ['events.EventList']

    def get_list_display_date(self):
        display_date = self.start_date.strftime('%B %e')
        if self.end_date:
            if self.end_date.year == self.start_date.year:
                if self.start_date.month == self.end_date.month:
                    display_date += self.end_date.strftime('-%e')
                else:
                    display_date += self.end_date.strftime(' - %B %e')
                display_date += self.start_date.strftime(' %Y')
            else:
                display_date += self.start_date.strftime(' %Y') + self.end_date.strftime(' - %B %e %Y')
        else:
            display_date += self.start_date.strftime(' %Y')
        return display_date

    def get_display_date(self):
        display_date = self.start_date.strftime('%B %e')
        if self.end_date:
            if self.start_date.month == self.end_date.month:
                display_date += self.end_date.strftime('-%e')
            else:
                display_date += self.end_date.strftime(' - %B %e')

        return display_date

    def get_context(self, request):
        context = super(Event, self).get_context(request)
        presentations_grouped = []

        group = []
        i = 1
        for presentation in self.event_presentations.all():
            group.append(presentation)
            if i % 3 == 0:
                presentations_grouped.append(group)
                group = []
            i += 1

        if (i-1) % 3 != 0:
            presentations_grouped.append(group)

        context['presentations'] = self.event_presentations.all()
        context['presentation_rows'] = presentations_grouped

        return context


class Presentations(Orderable, ClusterableModel):
    title = models.CharField(max_length=200)
    author = models.CharField(max_length=200)

    presenters = StreamField([
        ('contributor', ModelChooserBlock('people.Contributor')),
        ('custom_name', CharBlock(max_length=200))
    ], blank=True)

    summary = RichTextField(blank=True, null=True)
    event = ParentalKey('events.Event', related_name='event_presentations')

    panels = [
        FieldPanel('title'),
        FieldPanel('author'),
        StreamFieldPanel('presenters'),
        FieldPanel('summary'),
        FieldPanel('event'),
        InlinePanel('presentation_dates', label='Presentation date times')
    ]

    @property
    def first_published_at(self):
        """Return the event's first published date"""
        try:
            return self.event.first_published_at
        except Event.DoesNotExist:
            return None

class PresentationDate(models.Model):
    presentation = ParentalKey('events.Presentations', related_name='presentation_dates', on_delete=models.CASCADE)
    date_time = models.DateTimeField("Presentation date and time")

