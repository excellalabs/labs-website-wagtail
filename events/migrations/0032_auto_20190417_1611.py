# Generated by Django 2.1.4 on 2019-04-17 20:11

from django.db import migrations, models
import django.db.models.deletion
import modelcluster.fields


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0031_event_end_date'),
    ]

    operations = [
        migrations.CreateModel(
            name='PresentationDate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_time', models.DateTimeField(verbose_name='Presentation date and time')),
                ('presentation', modelcluster.fields.ParentalKey(on_delete=django.db.models.deletion.CASCADE, related_name='presentation_dates', to='events.Presentations')),
            ],
        ),
        migrations.AlterField(
            model_name='event',
            name='end_date',
            field=models.DateField(blank=True, null=True, verbose_name='Event end date'),
        ),
    ]
